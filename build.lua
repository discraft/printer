local args = { ... }

if #args < 1 then
    print("Usage:")
    print("printer <fileName>")
end

local file = fs.open(args[1], "r")
print("Printing " .. args[1] .. "...")
local data = file.readAll()

-- split string by delimiter
function string:split(delimiter)
    local result = {}
    local from = 1
    local delimFrom, delimTo = string.find(self, delimiter, from, true)
    while delimFrom do
        if (delimFrom ~= 1) then
            table.insert(result, string.sub(self, from, delimFrom - 1))
        end
        from = delimTo + 1
        delimFrom, delimTo = string.find(self, delimiter, from, true)
    end    
    if (from <= #self) then table.insert(result, string.sub(self, from)) end
    return result
end

function checkFuel()
    if turtle.getFuelLevel() < 32 then
        turtle.select(8)
        turtle.placeUp()
        turtle.suckUp(8)
        turtle.refuel()
        turtle.digUp()
    end
end

function checkItemCount(slot)
    if turtle.getItemCount() == 1 then
        turtle.select(slot + 4)
        turtle.placeUp()
        turtle.select(slot)
        turtle.suckUp(63)
        turtle.select(slot + 4)
        turtle.digUp()
        
        -- change back slot
        turtle.select(slot)
    end
end

-- for line in file
for i = 1, #(data:split("\n")) do
    local even = i % 2 == 0
    local row = data:split("\n")[i]
    
    -- if line number is even, reverse pattern
    if even then row = string.reverse(row) end

    for j = 1, #row do
        checkFuel()

        local n = string.sub(row,j,j)
        local slot = n + 1

        turtle.select(slot)
        checkItemCount(slot)
        turtle.placeDown()

        if (j ~= #row) then
            turtle.forward()
        end
    end

    if (i == #(data:split("\n"))) then
        break
    end

    -- turn direction depends on if the row is even or not
    if not even then
        turtle.turnRight()
        turtle.forward()
        turtle.turnRight()
    else
        turtle.turnLeft()
        turtle.forward()
        turtle.turnLeft()
    end
end
